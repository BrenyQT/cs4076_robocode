package CS4076._2023;

import robocode.*;

import java.awt.*;

public class _21315205_BrenyBOT extends AdvancedRobot {
    int moveDirection = 1;

   
    public void run() {
        setAdjustRadarForRobotTurn(true);
        setBodyColor(Color.magenta);
        setGunColor(Color.cyan);
        setRadarColor(Color.white);
        setScanColor(Color.white);
        setBulletColor(Color.blue);
        //sets the robot's gun to adjust independently of the robot's movement
        setAdjustGunForRobotTurn(true);
        //continuously turns the robot's radar to the right at the fastest possible speed
        turnRadarRightRadians(Double.POSITIVE_INFINITY);
    }


    public void onScannedRobot(ScannedRobotEvent e) {
        //calculates the absolute bearing of the enemy robot relative to the current heading of the tracking robot
        double absBearing = e.getBearingRadians() + getHeadingRadians();
        //calculates the lateral velocity of the enemy robot relative to the tracking robot
        double latVel = e.getVelocity() * Math.sin(e.getHeadingRadians() - absBearing);
        double gunTurnAmt;
        // sets the radar turn of the tracking robot to the opposite direction of its current radar turn.
        setTurnRadarLeftRadians(getRadarTurnRemainingRadians());


        //randomly change speed
        if (Math.random() > .9) {
            setMaxVelocity((12 * Math.random()) + 12);
        }

        //checking if scannned is a sentry
        if (e.isSentryRobot()) {
            moveDirection *= -1;
        }
        //checks if the distance between the robot and the enemy is greater than 150.
        else if (e.getDistance() > 150) {
            //takes the absolute bearing of the enemy, subtracts the current gun heading,
            // and adds the lateral velocity of the enemy divided by 22
            gunTurnAmt = robocode.util.Utils.normalRelativeAngle(absBearing - getGunHeadingRadians() + latVel / 22);
            // turns its gun by the calculated amount using the setTurnGunRightRadians method.
            setTurnGunRightRadians(gunTurnAmt);
            //robot can move towards the enemy while keeping its gun aimed at it.
            setTurnRightRadians(robocode.util.Utils.normalRelativeAngle(absBearing - getHeadingRadians() + latVel / getVelocity()));
            setAhead((e.getDistance() - 140) * moveDirection);
            setAhead(100 * moveDirection);
            setFire(3);//fire
        } else {
            //takes the absolute bearing of the enemy, subtracts the current gun heading,
            // and adds the lateral velocity of the enemy divided by 12
            gunTurnAmt = robocode.util.Utils.normalRelativeAngle(absBearing - getGunHeadingRadians() + latVel / 12);
            // turns its gun by the calculated amount using the setTurnGunRightRadians method.
            setTurnGunRightRadians(gunTurnAmt);
            //turn the robot perpendicular to the enemy.
            setTurnLeft(-90 - e.getBearing());
            setAhead((e.getDistance() - 140) * moveDirection);//move forward
            setAhead(100 * moveDirection);
            setFire(3);//fire
        }
    }

    public void onHitWall(HitWallEvent e) {
        moveDirection *= -1;
    }

    public void onHitRobot(HitRobotEvent e) {
        moveDirection *= -1;
    }
}